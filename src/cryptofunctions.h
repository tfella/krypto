// SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>
// SPDX-License-Identifier: GPL-2.0-or-later

#include <QObject>
#include <QQmlEngine>

class CryptoFunctions : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

public:
    CryptoFunctions(QObject *parent = nullptr);

    Q_INVOKABLE QString sha256(const QString &input) const;
};
