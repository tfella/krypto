// SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "transformations.h"

Transformations::Transformations(QObject *parent)
    : QAbstractListModel(parent)
{
}

QHash<int, QByteArray> Transformations::roleNames() const
{
    return {
        {NameRole, "name"},
        {AvailableTransformationRole, "availableTransformation"},
    };
}

QVariant Transformations::data(const QModelIndex &index, int role) const
{
    const auto row = index.row();
    const auto &transformation = m_transformations[row];
    if (role == NameRole) {
        return transformation.name;
    }
    return {};
}

int Transformations::rowCount(const QModelIndex &parent) const
{
    return m_transformations.size();
}

void Transformations::setInputBase64(const QString &inputBase64)
{
    m_inputBase64 = inputBase64;
    Q_EMIT inputChanged();
}

QString Transformations::inputBase64() const
{
    return m_inputBase64;
}

Q_INVOKABLE void Transformations::add(const AvailableTransformation &availableTransformation)
{
    beginInsertRows({}, rowCount(), rowCount());
    m_transformations += Transformation{
        .name = availableTransformation.name,
        .availableTransformation = availableTransformation,
    };
    endInsertRows();
}
