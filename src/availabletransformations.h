// SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QAbstractListModel>
#include <QQmlEngine>
#include <QString>

struct AvailableTransformation {
    Q_GADGET
    QML_ELEMENT

public:
    QString name;
    QString description;
};

class AvailableTransformations : public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

public:
    enum Roles {
        NameRole = Qt::DisplayRole,
        DescriptionRole,
        AvailableTransformationRole,
    };
    Q_ENUM(Roles);
    explicit AvailableTransformations(QObject *parent = nullptr);

    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role) const override;

    int rowCount(const QModelIndex &parent) const override;

Q_SIGNALS:
    void inputChanged();

private:
    QString m_inputBase64;
    QVector<AvailableTransformation> m_transformations;
};
