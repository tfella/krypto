// SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "availabletransformations.h"

using namespace Qt::Literals::StringLiterals;

AvailableTransformations::AvailableTransformations(QObject *parent)
    : QAbstractListModel(parent)
{
    m_transformations += AvailableTransformation{
        .name = QStringLiteral("Sha256"),
        .description = QStringLiteral("The sha256 hash algorithm"),
    };
    m_transformations += AvailableTransformation{
        .name = QStringLiteral("MD5"),
        .description = QStringLiteral("The md5 hash algorithm"),
    };
    m_transformations += AvailableTransformation{
        .name = QStringLiteral("AES"),
        .description = QStringLiteral("The aes symmetric encryption algorithm"),
    };
}

QHash<int, QByteArray> AvailableTransformations::roleNames() const
{
    return {
        {NameRole, "name"},
        {DescriptionRole, "description"},
        {AvailableTransformationRole, "availableTransformation"},
    };
}

QVariant AvailableTransformations::data(const QModelIndex &index, int role) const
{
    const auto row = index.row();
    const auto &transformation = m_transformations[row];
    if (role == NameRole) {
        return transformation.name;
    }
    if (role == DescriptionRole) {
        return transformation.description;
    }
    if (role == AvailableTransformationRole) {
        return QVariant::fromValue(transformation);
    }
    return {};
}

int AvailableTransformations::rowCount(const QModelIndex &parent) const
{
    return m_transformations.size();
}
