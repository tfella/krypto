// SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QAbstractListModel>
#include <QQmlEngine>
#include <QString>

#include "availabletransformations.h"

struct Transformation {
    QString name;
    AvailableTransformation availableTransformation;
};

class Transformations : public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(QString inputBase64 READ inputBase64 WRITE setInputBase64 NOTIFY inputChanged)

public:
    enum Roles {
        NameRole,
        AvailableTransformationRole,
    };
    Q_ENUM(Roles);
    explicit Transformations(QObject *parent = nullptr);

    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    void setInputBase64(const QString &inputBase64);
    QString inputBase64() const;

    Q_INVOKABLE void add(const AvailableTransformation &availableTransformation);

Q_SIGNALS:
    void inputChanged();

private:
    QString m_inputBase64;
    QVector<Transformation> m_transformations;
};
