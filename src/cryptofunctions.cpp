// SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "cryptofunctions.h"
#include <QCryptographicHash>

CryptoFunctions::CryptoFunctions(QObject *parent)
    : QObject(parent)
{}

QString CryptoFunctions::sha256(const QString &input) const
{
    return QString::fromLatin1(QCryptographicHash::hash(input.toLatin1(), QCryptographicHash::Sha256).toHex());
}
