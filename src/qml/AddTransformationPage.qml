// SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>
// SPDX-License-Identifier: GPL-2.0-or-later

import QtQuick
import QtQuick.Controls

import org.kde.kirigamiaddons.formcard as FormCard

import org.kde.krypto

FormCard.FormCardPage {
    id: root

    title: i18nc("@title", "Add Transformation")

    signal chosen(AvailableTransformation availableTransformation)

    FormCard.FormHeader {
        title: i18nc("@title", "Available Transformations")
    }
    FormCard.FormCard {
        Repeater {
            model: AvailableTransformations
            delegate: FormCard.FormButtonDelegate {
                text: model.name
                description: model.description
                onClicked: {
                    root.chosen(model.availableTransformation)
                    root.closeDialog()
                }
            }
        }
    }
}
