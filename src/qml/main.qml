// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>

import QtQuick
import QtQuick.Controls as Controls
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.formcard as FormCard
import org.kde.krypto

Kirigami.ApplicationWindow {
    id: root

    title: i18n("Krypto")

    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 20

    onClosing: App.saveWindowGeometry(root)

    onWidthChanged: saveWindowGeometryTimer.restart()
    onHeightChanged: saveWindowGeometryTimer.restart()
    onXChanged: saveWindowGeometryTimer.restart()
    onYChanged: saveWindowGeometryTimer.restart()

    Component.onCompleted: App.restoreWindowGeometry(root)

    Timer {
        id: saveWindowGeometryTimer
        interval: 1000
        onTriggered: App.saveWindowGeometry(root)
    }

    pageStack.initialPage: page

    FormCard.FormCardPage {
        id: page

        title: i18n("Krypto")

        FormCard.FormHeader {
            title: i18n("Input Data")
        }
        FormCard.FormCard {
            FormCard.FormTextFieldDelegate {
                id: inputField
                label: i18n("Data")
            }
        }

        FormCard.FormHeader {
            title: i18n("Transformations")
        }
        FormCard.FormCard {
            Repeater {
                model: Transformations {
                    id: transformations
                }
                delegate: FormCard.FormButtonDelegate {
                    text: model.name
                }
            }
            FormCard.FormDelegateSeparator {}
            FormCard.AbstractFormDelegate {
                contentItem: Kirigami.Icon {
                    source: "list-add"
                }
                onClicked: pageStack.pushDialogLayer(addTransformationPage, {}, {
                    title: i18nc("@title", "Add Transformation")
                })
            }
        }

        FormCard.FormHeader {
            title: i18n("Output Data")
        }
        FormCard.FormCard {
            FormCard.FormTextDelegate {
                text: i18n("Hex string")
                description: CryptoFunctions.sha256(inputField.text)
            }
        }
    }
    Component {
        id: addTransformationPage
        AddTransformationPage {
            onChosen: availableTransformation => transformations.add(availableTransformation)
        }
    }
}
